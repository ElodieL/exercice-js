
/*--------------------------CLASS--------------------------------*/

class Page {
    constructor(number, content, author) {
        this.number = number;
        this.content = content;
        this.author = author
    };
};

class Book {
    constructor(title, pages) {
    this.title = title;
    this.pages = pages;
    };

    addPage() {
        let newIndex = this.pages.length + 1;
        let newPage = new Page(
            newIndex,
            ('Citation de la page ' + newIndex + ' vide'),
            'inconnu',
        );
        this.pages.push(newPage);
        return console.log(this.pages);
    };

    editPage(page) {
        return console.log('edited page');
    }

    deletePage() {
        return console.log('deleted page');
    }

    GoToNextPage() {
        return console.log('next Page');
    };

    GoToPreviousPage() {
        return console.log('previous Page');
    };
};


/*--------------------------DATA--------------------------------*/

const myBook = new Book(
    'Mon livre de citations',
    [
        page1 = new Page(
            1,
            'Fais de ta vie un rêve, et d\'un rêve, une réalité.',
            'Antoine de Saint-Exupéry'
        ),
        page2 = new Page(
            2, 
            'La beauté est dans les yeux de celui qui regarde.',
            'Oscar Wilde'
        ),
        page3 = new Page(
            3, 
            'Le courage n\'est pas l\'absence de peur, mais la capacité de vaincre ce qui fait peur.',
            'Nelson Mandela'
        ),
        page4 = new Page(
            4, 
            'J\'ai pardonné des erreurs presque impardonnables, j\'ai essayé de remplacer des personnes irremplaçables et oublié des personnes inoubliables. J\'ai agi par impulsion, j\'ai été déçu par des gens que j\'en croyais incapables, mais j\'ai déçu des gens aussi. J\'ai tenu quelqu\'un dans mes bras pour le protéger. Je me suis fait des amis éternels. J\'ai ri quand il ne le fallait pas. J\'ai aimé et je l\'ai été en retour, mais j\'ai aussi été repoussé. J\'ai été aimé et je n\'ai pas su aimer. J\'ai crié et sauté de tant de joies, j\'ai vécu d\'amour et fait des promesses éternelles, mais je me suis brisé le coeur, tant de fois ! J\'ai pleuré en écoutant de la musique ou en regardant des photos. J\'ai téléphoné juste pour entendre une voix, je suis déjà tombé amoureux d\'un sourire. J\'ai déjà cru mourir par tant de nostalgie. J\'ai eu peur de perdre quelqu\'un de très spécial (que j\'ai fini par perdre). Mais j\'ai survécu ! Et je vis encore ! Et la vie, je ne m\'en lasse pas. Et toi non plus tu ne devrais pas t\'en lasser. Vis ! Ce qui est vraiment bon, c\'est de se battre avec persuasion, embrasser la vie et vivre avec passion, perdre avec classe et vaincre en osant... parce que le monde appartient à celui qui ose ! La vie est beaucoup trop belle pour être insignifiante.',
            'Charlie Chaplin'
            ),
        page5 = new Page(
            5, 
            'Etre libre, ce n\'est pas pouvoir faire ce que l\'on veut, mais c\'est vouloir ce que l\'on peut.',
            'Jean-Paul Sartre'
            ),
        page6 = new Page(
            6, 
            'La vie sans musique est tout simplement une erreur, une fatigue, un exil.',
            'Friedrich Nietzsche'
            ),

    ],
);
console.log(myBook);
console.log(myBook.pages);


/*-------------------------HTML ELEMENTS-----------------------------*/

//div
let pageHeader = document.getElementById('page-header');
let pageMain = document.getElementById('page-main');
let pageFooter = document.getElementById('page-footer');
let index = 0;
let maxPages = myBook.pages.length;

//Book Title
const BookTitle = (book) => {
    let title = document.createElement('h2');
    let newContent = book.title;
    title.textContent = newContent;
    return title;
};

//page content
let pageContent = document.createElement('p');
pageContent.textContent = (myBook.pages[index].content + ' ('+myBook.pages[index].author+')');

const BookPage = (book, index) => {
    let page = document.createElement('p');
    let newContent = book.pages[index].content;
    page.textContent = newContent;
    return page;
}

//page footer
let pageNumber = document.createElement('p');
pageNumber.textContent = (myBook.pages[index].number + '/' + maxPages);

//add button
let addButton = document.createElement('button');
addButton.textContent = 'Nouvelle page';
addButton.addEventListener( 'click', function(){
    myBook.addPage()
    pageNumber.textContent = (myBook.pages[index].number + '/' + myBook.pages.length);
    }
);

//next button
let nextButton = document.createElement('button');
nextButton.textContent = '>';
nextButton.addEventListener( 'click', function(){ 
        index++;
        pageContent.textContent = (myBook.pages[index].content + ' ('+myBook.pages[index].author+')') 
        pageNumber.textContent = (myBook.pages[index].number + '/' + myBook.pages.length);
    }
);

//previous button
let previousButton = document.createElement('button');
previousButton.textContent = '<';
previousButton.addEventListener( 'click', function(){ 
    index--;
    pageContent.textContent = (myBook.pages[index].content + ' ('+myBook.pages[index].author+')') 
    pageNumber.textContent = (myBook.pages[index].number + '/' + myBook.pages.length);
});


/*-----------------------------add in DOM----------------------------*/

pageHeader.append(BookTitle(myBook));
pageHeader.append(addButton);

pageMain.prepend(pageContent);
pageMain.prepend(BookPage(myBook, index))

pageFooter.prepend(previousButton);
pageFooter.append(pageNumber);
pageFooter.append(nextButton);

